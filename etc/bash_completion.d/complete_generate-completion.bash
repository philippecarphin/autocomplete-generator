#
# This file was generated by autocomplete-generator https://gitlab.com/philippecaphin/autocomplete-generator
#

# This is the function that will be called when we press TAB.
#
# Its purpose is to examine the current command line (as represented by the
# array COMP_WORDS) and to determine what the autocomplete should reply through
# the array COMPREPLY.
#
# This function is organized with subroutines who  are responsible for setting
# the 'generate_completion_compreply_candidates' variable.
#
# The compgen then filters out the candidates that don't begin with the word we are
# completing. In this case, if '--' is one of the words, we set empty candidates,
# otherwise, we look at the previous word and delegate # to candidate-setting functions
__complete_generate_completion() {

	COMPREPLY=()

	# We use the current word to filter out suggestions
	local cur="${COMP_WORDS[COMP_CWORD]}"
	local generate_completion_compreply_candidates=""
	__suggest_generate_completion_compreply_candidates >> ~/log.txt 2>&1


	# Compgen: takes the list of candidates and selects those matching ${cur}.
	# Once COMPREPLY is set, the shell does the rest.
	COMPREPLY=( $(compgen -W "${generate_completion_compreply_candidates}" -- ${cur}))
	echo "COMPREPLY = ${COMPREPLY[@]}" >> ~/log.txt

	return 0
}

__suggest_generate_completion_compreply_candidates(){
	# We use the current word to decide what to do
	local cur="${COMP_WORDS[COMP_CWORD]}"
	if __generate_completion_dash_dash_in_words ; then
		return
	fi

	option=$(__generate_completion_get_current_option)
	if [[ "$option" != "" ]] ; then
		__suggest_generate_completion_args_for_option ${option}
	else
		# Suggest from filesystem if current word doesn't start with '-'
		# if ! [[ "$cur" = -* ]] ; then
		# 	return
		# fi
		__suggest_generate_completion_options
	fi
}

__generate_completion_dash_dash_in_words(){
	for ((i=0;i<COMP_CWORD-1;i++)) ; do
		w=${COMP_WORDS[$i]}
		if [[ "$w" == "--" ]] ; then
			return 0
		fi
	done
	return 1
}

__generate_completion_get_current_option(){
	# The word before that
	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	if [[ "$prev" == -* ]] ; then
		echo "$prev"
	fi
}

__suggest_generate_completion_options(){
	generate_completion_compreply_candidates=" --options-file --command"
}

__suggest_generate_completion_args_for_option(){
	case "$1" in
		--options-file) __suggest_generate_completion_key_options_file_values ;;
		--command) __suggest_generate_completion_key_command_values ;;
	esac
}

__suggest_generate_completion_key_options_file_values(){
	generate_completion_compreply_candidates=""
}

__suggest_generate_completion_key_command_values(){
	generate_completion_compreply_candidates=""
}

complete -o default -F __complete_generate_completion generate-completion
