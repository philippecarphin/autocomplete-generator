#!/usr/bin/env python3
import argparse
import sys
import subprocess
import os

def main():

    output_file = sys.stdout
    p = argparse.ArgumentParser(description="""
        Generate a bash autocompletion
        script for COMMAND according to OPTIONS_FILE with lines of the form
            '--color red green blue'
        with an option as the first word and the possible values for that option
        following on the same line.  The program outputs an autocomplete script
        to STDOUT.  You can quick-try it by doing 'eval "$(autocomplte-generator -o options.txt -c my_command)"
        and then try doing tab completion on my_command and options""")
    p.add_argument("--options-file", "-o", help="Options file with 'option values ...' on each line")
    p.add_argument("--command", "-c", help="Name of command : Used in the generated script and may be used to try to run the command with '-h' if no options file is provided.", required=True)

    args = p.parse_args()
    args.scriptnom = os.path.basename(args.command).replace('-','_')

    data = get_data(args)

    output_file = sys.stdout

    print_start_part(args, output_file)

    print_options_suggester(args, data, output_file)
    print_optarg_suggester_switch(args, data, output_file)
    print_individual_key_suggesters(args, data, output_file)
    print_setup_command(args, output_file)

def get_data(args):
    """ Get data on program options either through an options file or if
    the help text of the program uses CCLARGS notation, we can get it that way
    """
    if args.options_file:
        return read_options_file(args.options_file)

    result = subprocess.run([args.command, "-h"], universal_newlines=True, stderr=subprocess.PIPE, stdout=subprocess.DEVNULL)
    if "SEQUENCE D'APPEL" in result.stderr:
        lines = filter(lambda l: l.startswith(" IN"), result.stderr.splitlines())
        return {line.split()[1]: [] for line in lines}

    raise Exception(f"No way to get options for {args.command}")

def print_start_part(args, output_file):
    """Predefined functions with args.command in their names"""
    print(start.format(scriptnom=args.scriptnom), file=output_file)

def read_options_file(filename):
    """Return a dictionary where keys are options and values are possible values
    for the option"""
    with open(filename, 'r') as f:
        lines = (l.strip() for l in f.read().splitlines())

    data = {}
    for l in lines:
        if l == '':
            continue
        if l.startswith("#"):
            continue
        words = l.split()
        data[words[0]] = words[1:]

    return data

def print_options_suggester(args, data, output_file):
    """ Print one function looking like
        __suggest_my_command_options(){
            echo ="<space-separated list of options>"
        }
    """
    print("""__suggest_{scriptnom}_options(){{\n\techo " {candidates}"\n}}\n"""
            .format(scriptnom=args.scriptnom,
                candidates=' '.join([f'{o}' for o in data])),
                file=output_file)

def print_optarg_suggester_switch(args, data, output_file):
    """ Print a switch-case statement that delegates to the right function
    depending on the key
        __suggest_my_command_args_for_option(){
            case "$1" in
                --color) __suggest_my_command_args_for_color ;;
                --day-of-week) __suggest_my_command_args_for_day_of_week ;;
            esac
        }
    """
    print("""__suggest_{scriptnom}_args_for_option(){{\n\tcase "$1" in"""
            .format(scriptnom=args.scriptnom, file=output_file))

    for opt in data:
        print(f"""\t\t{opt}) __suggest_{args.scriptnom}_key_{opt.strip("-").replace("-","_")}_values ;;""", file=output_file)

    print("""\tesac\n}\n""", file=output_file)

def print_individual_key_suggesters(args, data, output_file):
    """ Prints the individual functions that suggest values for keys looking like
        __suggest_my_command_key_day_of_week_values(){
            echo "Monday Tuesday Wednesday Thursday Friday Saturday Sunday"
        }
    NOTE: These can be modified any way we want in the resulting script.  It can
    do

            some-command ${COMP_WORDS})

    to send the curren command line to another program to obtain candidates in a
    sophisticated way.

            git branch | tr '*' ''

    would be a simple way to get branch names for arguments to an option that
    takes a branch name as an argument.
    """
    for opt in data:
        print("""__suggest_{}_key_{}_values(){{\n\techo "{}"\n}}\n"""
                .format(
                    args.scriptnom,
                    opt.strip("-").replace("-","_"),
                    ' '.join(data[opt])), file=output_file)

def print_setup_command(args, output_file):
    """ This simply sets up the first function as the function that BASH should
    use to complete the command.

    The '-o default' makes it so that if the function produces an empty COMPREPLY
    BASH will fall back to regular filename completion"""

    print("complete -o default -F __complete_{scriptnom} {command}"
            .format(
                scriptnom=args.scriptnom,
                command=os.path.basename(args.command)
            ), file=output_file)

start = """#
# This file was generated by autocomplete-generator https://gitlab.com/philippecaphin/autocomplete-generator
#

# This is the function that will be called when we press TAB.
#
# Its purpose is to examine the current command line (as represented by the
# array COMP_WORDS) and to determine what the autocomplete should reply through
# the array COMPREPLY.
#
# This function is organized with subroutines who  are responsible for setting
# the '{scriptnom}_compreply_candidates' variable.
#
# The compgen then filters out the candidates that don't begin with the word we are
# completing. In this case, if '--' is one of the words, we set empty candidates,
# otherwise, we look at the previous word and delegate # to candidate-setting functions
__complete_{scriptnom}() {{

	COMPREPLY=()

	# We use the current word to filter out suggestions
	local cur="${{COMP_WORDS[COMP_CWORD]}}"


	# Compgen: takes the list of candidates and selects those matching ${{cur}}.
	# Once COMPREPLY is set, the shell does the rest.
	COMPREPLY=( $(compgen -W "$(__suggest_{scriptnom}_compreply_candidates 2> ~/.autocomplete_log.txt)" -- ${{cur}}))

	return 0
}}

__suggest_{scriptnom}_compreply_candidates(){{
	# We use the current word to decide what to do
	local cur="${{COMP_WORDS[COMP_CWORD]}}"
	if __{scriptnom}_dash_dash_in_words ; then
		return
	fi

	option=$(__{scriptnom}_get_current_option)
	if [[ "$option" != "" ]] ; then
		__suggest_{scriptnom}_args_for_option ${{option}}
	else
		# Suggest from filesystem if current word doesn't start with '-'
		# if ! [[ "$cur" = -* ]] ; then
		# 	return
		# fi
		__suggest_{scriptnom}_options
	fi
}}

__{scriptnom}_dash_dash_in_words(){{
	for ((i=0;i<COMP_CWORD-1;i++)) ; do
		w=${{COMP_WORDS[$i]}}
		if [[ "$w" == "--" ]] ; then
			return 0
		fi
	done
	return 1
}}

__{scriptnom}_get_current_option(){{
	# The word before that
	local prev="${{COMP_WORDS[COMP_CWORD-1]}}"
	if [[ "$prev" == -* ]] ; then
		echo "$prev"
	fi
}}
"""


if __name__ == '__main__':
    main()
