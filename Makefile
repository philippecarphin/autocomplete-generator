include util/colors.mk

PREFIX ?= $(PWD)/localinstall

ifeq ($(shell uname),Darwin)
	INSTALL = ginstall
else
	INSTALL = install
endif

all: man

install: all
	$(INSTALL) scripts/autocomplete-generator.py -D $(DESTDIR)$(PREFIX)/bin/generate-completion
	$(INSTALL) share/man/man1/generate-completion.1 -D $(DESTDIR)$(PREFIX)/share/man/man1/generate-completion.1
	$(INSTALL) etc/bash_completion.d/complete_generate-completion.bash -D $(DESTDIR)$(PREFIX)/etc/bash_completion.d/complete_generate-completion.bash

ssm:
	spkg-buildpackage --name autocomplete-generator --version 1.0.0 --description "Generate autocomplete scripts for BASH" --sourced-file etc/bash_completion.d/complete_generate-completion.bash
	spkg-trypackage *.ssm

man: share/man/man1/generate-completion.1
%.1:%.org
	pandoc -s -f org -t man $< -o $@

# TESTING TARGETS
check: check_gencompletion check_cclargs_completion

check_gencompletion: test/generated_completion.bash
	$(call make_echo_run_test,"Comparing $< to reference file")
	$(at) diff $< test/ref_gencompletion.bash
	$(call success)
test/generated_completion.bash: scripts/autocomplete-generator.py test/options.txt
	$(call make_echo_generate_file)
	$(at) python3 $< -o test/options.txt --command test/my_command >$@

check_cclargs_completion: test/generated_cclargs_completion.bash
	$(call make_echo_run_test,"Comparing $< to reference file")
	$(at) diff $< test/ref_cclargs_completion.bash
	$(call success)
test/generated_cclargs_completion.bash: scripts/autocomplete-generator.py test/my_command
	$(call make_echo_generate_file)
	$(at) python3 $< --command ./test/my_command >$@

# Cleanup
clean:
	$(at) rm -f test/generated*
	$(at) rm -rf autocomplete-generator_*_all
	$(at) rm -rf autocomplete-generator_*_all.ssm
	$(at) rm -rf autocomplete-generator_*.ssmd
	$(at) rm -f share/man/man1/*.1

